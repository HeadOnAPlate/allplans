(function ($) {
    $(document).ready(function() {
        var hideNeverExecuted = $('#hideNeverExecuted');
        hideNeverExecuted.change( function() {
            var checked = $(this).is(':checked');
            hideOrShowRow( '.Suspended, .NeverExecuted', !checked );
        });

        $('#hideUnknown').change( function() {
            var checked = $(this).is(':checked');
            hideOrShowRow( '.Unknown', !checked );
        });

        var hideSuccessful = $('#hideSuccessful');
        hideSuccessful.change( function() {
            var checked = $(this).is(':checked');
            hideOrShowRow( '.Successful', !checked );
        });

        $('#changeBranch').change( function() {
            var branchName = $(this).val();
            if( branchName.indexOf( '-- ')==0 ) {
                branchName = '';
            }
            window.location = updateQueryStringParameter( window.location.href, "filter", branchName );
        });
        hideNeverExecuted.trigger('change');
        hideSuccessful.trigger('change');
    });

    function updateQueryStringParameter(uri, key, value) {
        var re = new RegExp("([?|&])" + key + "=.*?(&|$)", "i");
        separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            return uri + separator + key + "=" + value;
        }
    }

    function isHidden( el ) {
        return $(el).css("display") == "none";
    }

    function hideOrShowRow( classes, show ) {
        var elements = $(classes).parent();
        var tables = elements.parent().parent();
        if( show ) {
            elements.show();
        } else {
            elements.hide();
        }

        tables.each( function( index, tableEl ) {
            var table = $(tableEl);
            var numberOfHiddenRows = 0;
            var allRows = table.children('tbody').children('tr');
            allRows.each( function( index, row ) {
                if( isHidden( row ) ) {
                    numberOfHiddenRows++;
                }
            });
            if( numberOfHiddenRows == allRows.size() ) {
                table.hide();
            } else {
                table.show();
            }

        } );

        tables.each( function( index, tableEl) {
            var table = $(tableEl);
            // Now also check if all tables for a project are hidding
            var parentDiv = table.parent( "div" );
            var allTables = parentDiv.children( "table" );
            var numberOfHiddenTables = 0;
            allTables.each( function( index, table ) {
                if( isHidden(table) ) {
                    numberOfHiddenTables++;
                }
            });
            if( numberOfHiddenTables == allTables.size() ) {
                parentDiv.prev().hide();
            } else {
                parentDiv.prev().show();
            }
        });

    }
})(jQuery);